var test = angular.module("testApp", ['ngRoute']);

test.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl : 'login'
		})
		.when('/error', {
			templateUrl : '/'
		})
		.when('/home', {
			templateUrl : '/app/partial/home.html'
		})
		.when('/companies', {
			templateUrl : '/app/partial/companies.html'
		})
		.when('/companies/edit/:id', {
			templateUrl : '/app/partial/update_company.html'
		})
		.when('/companies/new', {
			templateUrl : '/app/partial/new_company.html'
		})
		.when('/items', {
			templateUrl : '/app/partial/items.html'
		})
		.when('/users', {
			templateUrl : '/app/partial/users.html'
		})
		.otherwise({
			redirectTo: '/app/partial/home.html'
		});
}]);

test.controller("companiesCtrl", function($scope, $http, $location){
	
	var baseUrl = "/companies";
	
	$scope.company = [];
	
	$scope.pageNum = 0;
	$scope.totalPages= 1;
	
var getCompanies = function(){
		
		var conf = {params: {}};
		
		conf.params.pageNum = $scope.pageNum;
		
		var promise = $http.get(baseUrl, conf);
		promise.then(
			function success(answ){
				$scope.company = answ.data;
				$scope.totalPages=answ.headers("totalPages");
			},
			function error(answ){
				alert("Something went wrong getting all companies data!");
			}
		);
	}


getCompanies();

$scope.changePage = function(direction){
	$scope.pageNum = $scope.pageNum + direction;
	getCompanies();
}

$scope.deleteCompany = function (id){
	
	$http.delete(baseUrl + "/" + id).then(
		function success(answ){
			getCompanies();
		},
		function error(answ){
			alert("Couldn't delete company!");
		}	
	);
}

// za edit na posebnoj stranici
$scope.goToEdit = function(id){
	$location.path('/companies/edit/' + id);
}

// za dodavanje na posebnoj stranici
$scope.goToAdd = function(){
	$location.path('/companies/new');
}

});

// kontroler za edit u posebnoj stranici
test.controller("editCompanyCtrl", function($scope, $routeParams, $http, $location){
	
	var baseUrl = "/companies";
	var id = $routeParams.id;
	
	$scope.updatedCompany = {};
	
	$scope.getCompanybyId = function(){
		
		$http.get(baseUrl + "/" + id).then(
			function success(answ){
				$scope.updatedCompany = answ.data;
			},
			function error(answ){
				alert('Something went wrong getting company!');
			}
		);
	}

	$scope.getCompanybyId();

	$scope.edit = function(){
	
		$http.put(baseUrl + "/" + id, $scope.updatedCompany).then(
			function success(answ){
				$location.path("/companies");
				},
			function error(answ){
				alert('Something went wrong editing!');
				}
		);
	}

});

test.controller("newCompanyCtrl", function($scope, $http, $location){
	
	var baseUrl = "/companies";
	
	$scope.company = [];
	
	$scope.newCompany = {};
	$scope.newCompany.name = "";
	$scope.newCompany.address = "";
	$scope.newCompany.validLicenceTill = "";
	$scope.newCompany.contactPerson = "";
	
		
	$scope.addCompany = function(){

		var promise = $http.post(baseUrl, $scope.newCompany);
			promise.then(
				function success(answ){
					$location.path("/companies");
				},
				function error(answ){
					alert("Something went wrong adding company!");
				}
			);
		}
});
